﻿#include <iostream>
void FindOdd(int N, int k)
{
	for (int i = k; i < N; i += 2)
		std::cout << i << " \n";

}		
int main()
{
	const int N = 10;
	std::cout << "Even:\n";
	FindOdd(N, 0);
	std::cout << "Odd:\n";
	FindOdd(N, 1);
}
